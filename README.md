# Open Mobility indicator Jupyter notebook "Trips per day at and population 350m around stops"

## description : [indicator.yml file](https://gitlab.com/open-mobility-indicators/indicators/stop_count/-/blob/main/indicator.yaml)

This indicator calculates the number of trips passing by each publich transport stop

forked from [jupyter notebook template](https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook)

[Install locally using a venv or Docker, Build and Use (download, compute)](https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook/-/blob/main/README.md#jupyter-notebook-indicator-template)  

